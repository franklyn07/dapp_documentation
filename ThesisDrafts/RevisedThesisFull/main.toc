\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {subsection}{\numberline {1.1}Aim}{2}
\contentsline {subsection}{\numberline {1.2}Objectives}{3}
\contentsline {subsection}{\numberline {1.3}Achievements}{4}
\contentsline {section}{\numberline {2}Literature Review}{5}
\contentsline {subsection}{\numberline {2.1}Background}{5}
\contentsline {subsubsection}{\numberline {2.1.1}Blockchain Technologies}{5}
\contentsline {subsubsection}{\numberline {2.1.2}Ethereum Network}{5}
\contentsline {subsubsection}{\numberline {2.1.3}DApps, DApp Browsers, and Wallets}{6}
\contentsline {subsubsection}{\numberline {2.1.4}E-Voting DApp}{7}
\contentsline {subsubsection}{\numberline {2.1.5}Chrome Devtools Protocol}{8}
\contentsline {subsubsection}{\numberline {2.1.6}Attacks Targeting Blockchain Infrastructure}{9}
\contentsline {subsection}{\numberline {2.2}Related Work}{10}
\contentsline {section}{\numberline {3}Threat Analysis}{13}
\contentsline {subsection}{\numberline {3.1}Vulnerabilities in the DApp's Application Functionality}{13}
\contentsline {subsection}{\numberline {3.2}Vulnerabilities in the DApp's Cryptocurrency Functionality}{14}
\contentsline {subsection}{\numberline {3.3}Implications of DApp Browsers on Identified Vulnerabilities}{14}
\contentsline {subsection}{\numberline {3.4}Proof-of-Concept Malware}{16}
\contentsline {subsubsection}{\numberline {3.4.1}Tamper Application Functionality of E-Voting DApp}{16}
\contentsline {subsubsection}{\numberline {3.4.2}Forge Application Functionality of E-Voting DApp}{17}
\contentsline {subsubsection}{\numberline {3.4.3}Tamper Cryptocurrency Functionality of E-Voting DApp:\\Using Malicious Contract Account}{18}
\contentsline {subsubsection}{\numberline {3.4.4}Tamper Cryptocurrency Functionality of E-Voting DApp:\\Using Externally Owned Account}{19}
\contentsline {subsubsection}{\numberline {3.4.5}Forge Cryptocurrency Functionality of E-Voting DApp:\\Contract Account and External Account}{19}
\contentsline {section}{\numberline {4}DAppInvestigator}{19}
\contentsline {subsection}{\numberline {4.1}Proposed Technique}{20}
\contentsline {subsection}{\numberline {4.2}Implementation}{22}
\contentsline {subsubsection}{\numberline {4.2.1}DAppInvestigator: Chrome Extension}{22}
\contentsline {subsubsection}{\numberline {4.2.2}DAppInvestigator: DevTools Protocol}{24}
\contentsline {subsubsection}{\numberline {4.2.3}Implementation Details}{26}
\contentsline {section}{\numberline {5}Evaluation}{27}
\contentsline {subsection}{\numberline {5.1}Effectiveness}{27}
\contentsline {subsubsection}{\numberline {5.1.1}Tamper Application Functionality of E-Voting DApp}{28}
\contentsline {subsubsection}{\numberline {5.1.2}Forge Application Functionality of E-Voting DApp }{28}
\contentsline {subsubsection}{\numberline {5.1.3}Tamper Cryptocurrency Functionality of E-Voting DApp:\\Using Malicious Contract Account}{29}
\contentsline {subsubsection}{\numberline {5.1.4}Tamper Cryptocurrency Functionality of E-Voting DApp:\\Using Externally Owned Account}{29}
\contentsline {subsubsection}{\numberline {5.1.5}Forge Cryptocurrency Functionality of E-Voting DApp:\\Contract Account and External Account}{30}
\contentsline {subsubsection}{\numberline {5.1.6}Trigger Points Evaluation: TCB using Full Browser Stack}{30}
\contentsline {subsubsection}{\numberline {5.1.7}Trigger Points: TCB using DevTools Protocol Code Base}{32}
\contentsline {subsection}{\numberline {5.2}Time Overheads}{32}
\contentsline {subsection}{\numberline {5.3}Comparing Implementations}{34}
\contentsline {section}{\numberline {6}Conclusions and Future Work}{35}
