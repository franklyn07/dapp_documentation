\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.75\linewidth]{./images/threatAnalysis.png}
  \caption[Threat Analysis - DApp Architecture]{Threat Analysis - DApp Architecture\index{tamperAttack}}
  \label{fig:archi}
\end{figure}
\efloatseparator
 
\begin{figure}[ht!]
         \centering
         \begin{subfigure}[b]{0.63\textwidth}
             \centering
             \includegraphics[width=\textwidth]{./images/tamperApplicationTransaction}
             \caption{Tamper Application TX}
             \label{fig:tamperApplAttack}
         \end{subfigure}
         \hfill
         \begin{subfigure}[b]{0.36\textwidth}
             \centering
             \includegraphics[width=\textwidth]{./images/forgeApplicationTransaction}
             \caption{Forge Application TX}
             \label{fig:forgeApplAttack}
         \end{subfigure}
            \caption{DApp Exploitation - Application Functionality}
            \label{fig:applicationAttack}
    \end{figure}

    \subsection{Vulnerabilities in the DApp's Cryptocurrency Functionality}

    This set of potential vulnerabilities is found within the DApp's capability of receiving and/or making cryptocurrency transfers. In Figure~\ref{fig:cryptoTransferAttack}, we show how malware would interact with a DApp in order to override the DApp's functionality, with the ultimate goal being that of misappropriation of user funds, obfuscated by an authentic DApp's front-end. The attack would have the DApp's front-end redirect cryptocurrency trans
