--Ethereum Basics--
1) https://medium.com/blockchannel/life-cycle-of-an-ethereum-transaction-e5c66bae0f6e
2) https://medium.com/@jgm.orinoco/releasing-stuck-ethereum-transactions-1390149f297d
3) https://medium.com/@mvmurthy/ethereum-for-web-developers-890be23d1d0c
4) https://medium.com/swlh/how-does-bitcoin-blockchain-mining-work-36db1c5cb55d - Read
5) https://www.quora.com/What-is-distributed-consensus-in-Bitcoin#
6) https://github.com/ethereum/wiki/wiki/White-Paper - Read
7) https://etherworld.co/2017/02/14/concept-of-merkle-tree-in-ethereum/ - Read
8) https://www.youtube.com/watch?v=V6gLY-1G4Mc - Watched
9) https://medium.com/coinmonks/ethereum-account-212feb9c4154
10) https://medium.com/@preethikasireddy/how-does-ethereum-work-anyway-22d1df506369
11) https://www.coindesk.com/information/how-ethereum-works/ - Read
12) https://ethereum.gitbooks.io/frontier-guide/content/contracts_and_transactions_intro.html
13) https://coinsutra.com/ethereum-beginners-guide/
14) https://blog.ethereum.org/2015/11/15/merkling-in-ethereum/ - Read
15) https://blockgeeks.com/guides/smart-contracts/
16) https://www.investopedia.com/terms/d/distributed-ledgers.asp - Read
17) https://www.youtube.com/watch?v=SSo_EIwHSd4 - Watched
18) https://www.youtube.com/watch?v=zVqczFZr124 - CREATE BLOCKCHAIN IN JS
19) https://www.coindesk.com/information/what-is-ethereum/ - Read
20) https://github.com/ethereum/wiki/wiki/Patricia-Tree
21) https://easythereentropy.wordpress.com/2014/06/04/understanding-the-ethereum-trie/
22) https://medium.com/ibbc-io/ethereum-uncles-how-family-makes-you-stronger-d6e7aaef7b2b - Read
23) https://blog.wetrust.io/why-do-i-need-a-public-and-private-key-on-the-blockchain-c2ea74a69e76 - Read
24) https://learnxinyminutes.com/docs/solidity/ - Solidity quick tutorial
	https://www.bitdegree.org/learn/solidity-comments/
25) https://truffleframework.com/tutorials/pet-shop - Worked Out
26) https://blockgeeks.com/guides/ethereum-gas-step-by-step-guide/ - Read

--Truffle and Solidiyt--
1) https://truffleframework.com/docs/truffle/overview
2) https://www.bitdegree.org/learn/solidity-comments/
3) https://learnxinyminutes.com/docs/solidity/
4) https://karl.tech/learning-solidity-part-1-deploy-a-contract/ - suggested by metamask devs

--Large Insights on BlockChain--
https://www.emeraldinsight.com/doi/full/10.1108/APJIE-12-2017-034

--JS and JQuery--
1) https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics
2) https://blog.cloudboost.io/best-jquery-tutorials-to-help-you-master-it-in-just-few-hours-2ce1be915bd5
3) https://spring.io/understanding/javascript-promises - Read
4) https://scotch.io/tutorials/javascript-promises-for-dummies

--MetaMask--
1) https://medium.com/metamask/developing-ethereum-dapps-with-truffle-and-metamask-aa8ad7e363ba
2) https://github.com/MetaMask/metamask-extension
3) https://medium.com/coinmonks/metamask-introduction-f89ac80bd30f - Read
4) https://rados.io/what-is-metamask/amp/ - Read
5) https://www.bitdegree.org/tutorials/metamask/ - Read
6) https://cryptocurrencyfacts.com/metamask-explained/ - Read
7) https://ethereum.stackexchange.com/questions/39954/does-metamask-store-private-key-on-server-or-anywhere-else - Read
8) https://coinsutra.com/hd-wallets-deterministic-wallet/ - Read
9) https://github.com/MetaMask/metamask-extension/issues/328

--Bip32--
1) https://silentcicero.gitbooks.io/pro-tips-for-ethereum-wallet-management/content/ethereum-wallet-basics/using-seed-phrases-to-create-ethereum-accounts.html - Read
2) https://ethereum.stackexchange.com/questions/49462/how-metamask-restores-account-information-using-seed-words - Read
3) https://silentcicero.gitbooks.io/pro-tips-for-ethereum-wallet-management/content/ethereum-wallet-basics/what-is-an-ethereum-wallet.html - Read
4) https://coinsutra.com/hd-wallets-deterministic-wallet/ - Read
5)https://medium.com/bitcraft/hd-wallets-explained-from-high-level-to-nuts-and-bolts-9a41545f5b0 - Read

--Infura--
1) https://media.consensys.net/why-infura-is-the-secret-weapon-of-ethereum-infrastructure-af6fc7c77052
2) https://stackoverflow.com/questions/49291445/what-is-the-of-use-web3-providers-httpprovider - Read

--web3 Providers--
1) https://medium.com/coinmonks/solidity-and-web3-js-141115b0f8c5 - Read

--voting dapp resources--
1) https://medium.com/@mvmurthy/full-stack-hello-world-voting-ethereum-dapp-tutorial-part-3-331c2712c9df
2) https://medium.com/@mvmurthy/full-stack-hello-world-voting-ethereum-dapp-tutorial-part-2-30b3d335aa1f
3) https://medium.com/@mvmurthy/full-stack-hello-world-voting-ethereum-dapp-tutorial-part-1-40d2d0d807c2 (not so much used)

--Chrome Extensions--
1) https://developer.chrome.com/extensions
2) https://developer.chrome.com/extensions/overview
3) https://developer.chrome.com/extensions/getstarted

--BIP39 VImp--
1) https://www.youtube.com/watch?v=hRXcY_tIlrw

--Contents of KeyStore When Hosting Node--
1)https://medium.com/@julien.maffre/what-is-an-ethereum-keystore-file-86c8c5917b97

--How Migrations Contract Works--
1)https://medium.com/@blockchain101/demystifying-truffle-migrate-21afbcdf3264

--Rinkeby, Rinkeby Faucet and EtherScan Rinkeby--
1) https://faucet.rinkeby.io/
2) https://www.rinkeby.io/#stats
3) https://rinkeby.etherscan.io/

--MetaMask GitHub Repo--
1) Add Accounts / Importing Accounts - https://github.com/MetaMask/eth-simple-keyring

--EthereuemJS API--
1) https://github.com/ethereumjs

--Bip32--
1) https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki

--Bip39--
1) https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki

--Private Key vs Public Key--
1) https://ethereum.stackexchange.com/questions/33171/ethereum-address-vs-public-key
2) https://ethereum.stackexchange.com/questions/3542/how-are-ethereum-addresses-generated
3) http://ethdocs.org/en/latest/account-management.html?highlight=address#keyfiles

--Signing transactions in metamask and geth--
1) https://hackernoon.com/a-closer-look-at-ethereum-signatures-5784c14abecc
2) https://ethereum.stackexchange.com/questions/15766/what-does-v-r-s-in-eth-gettransactionbyhash-mean
3) https://medium.com/@angellopozo/ethereum-signing-and-validating-13a2d7cb0ee3
4) https://ethereum.stackexchange.com/questions/13778/get-public-key-of-any-ethereum-account

--BIPS IN GENERAL __ REREAD-- 
1) https://www.reddit.com/r/Bitcoin/comments/2srxr2/what_is_the_difference_between_bip_32_and_bip_44/
2) http://aaronjaramillo.org/bip-44-hierarchical-deterministic-wallets

--Local_Storage_Chrome--
1) https://scotch.io/@PratyushB/local-storage-vs-session-storage-vs-cookie
2) https://stackoverflow.com/questions/5523140/html5-local-storage-vs-session-storage
3) https://dev.to/rdegges/please-stop-using-local-storage-1i04

--Metamask_Extension_Manifest--
1) https://github.com/oraclesorg/metamask-plugin/blob/master/app/manifest.json

--Infura Api Docs--
1) https://infura.io/docs 

--MetaMask Curated BlackList--
1) https://github.com/MetaMask/eth-phishing-detect/blob/master/src/config.json

--Interacting with contract using web3 SCRIPT INJECTION --
1) https://medium.com/coinmonks/interacting-with-ethereum-smart-contracts-through-web3-js-e0efad17977

--Paper Resources--
1) https://ieeexplore.ieee.org/abstract/document/8543395
2) https://link.springer.com/chapter/10.1007/978-1-4842-4075-5_8
3) https://www.sciencedirect.com/science/article/pii/S0167739X17318332

--Metamask Web3 Provider Shared With External Extensions--
1) https://github.com/MetaMask/metamask-extension/issues/4577
2) https://github.com/SuperuserLabs/thankful/issues/12

--Malware and Similar Attacks--
1) https://securityintelligence.com/news/razy-trojan-installs-malicious-browser-extensions-to-steal-cryptocurrency/
2) https://securelist.com/razy-in-search-of-cryptocurrency/89485/
3) https://www.fortinet.com/blog/threat-research/copy-pasting-thief-from-a-copy-pasted-code.html
4) https://blog.ensilo.com/darkgate-malware
5) https://money.cnn.com/2018/01/29/technology/coincheck-cryptocurrency-exchange-hack-japan/index.html
6) https://ethereum.stackexchange.com/questions/3887/how-to-reduce-the-chances-of-your-ethereum-wallet-getting-hacked

--Incident Response--
1) https://www.securitymetrics.com/blog/6-phases-incident-response-plan
2) https://www.varonis.com/blog/incident-response-plan/

--DOM--
1) https://medium.com/javascript-in-plain-english/how-to-traverse-the-dom-in-javascript-d6555c335b4e

--Browser Lifecycle--
1) https://developers.google.com/web/updates/2018/09/inside-browser-part1
2) https://developers.google.com/web/updates/2018/09/inside-browser-part2 
3) https://developers.google.com/web/updates/2018/09/inside-browser-part3
4) https://developers.google.com/web/updates/2018/09/inside-browser-part4

--Page Lifecycle--
1) https://developers.google.com/web/updates/2018/07/page-lifecycle-api

--To make restful api that saves evidence--
1) https://codeforgeek.com/2014/09/handle-get-post-request-express-4/

--Performance vs. Date--
1) https://www.html5rocks.com/en/tutorials/webperformance/basics/

--Measuring Function Execution--
1) https://www.sitepoint.com/measuring-javascript-functions-performance/

--DevToolsProtocol--
1) https://www.nativescript.org/blog/chrome-devtools-integration
2) https://github.com/cyrus-and/chrome-remote-interface/issues/64
3) https://chromedevtools.github.io/devtools-protocol/tot/Page
4) https://groups.google.com/forum/#!forum/chrome-debugging-protocol

--Browser extension threats--
1) https://securityboulevard.com/2019/01/browser-extensions-can-pose-significant-cyber-security-threats/

--Mist and geth--
1) https://ethereum.stackexchange.com/questions/35971/is-geth-a-wallet-like-mist

metamask ease of use
geth more advanced users - state problem of fast sync and latest block 
investigate hardware wallets and how adapted to ethereum .. and other means of connecting eg. mist still zoom on metamask and geth - write about advantage of using metamask - geth takes a lot t sync even with fast sync - loads of memory etc. hit on practicality and security trade off - why use metamask and not geth simple user vs advanced user scoping on desktop 
write about bips 32 39 and 44  why address was developed copy and paste malware

focus on chrome plugins - metamask
	how chrome plugins can inject scripts inside a page 
	how how the html communicates with a plugin
	how plugins communicate with eachother


--https://ambcrypto.com/metamask-stop-injecting-web3-soon-positive-news-developers-end-users/
--https://medium.com/coinmonks/best-dapp-browser-of-2018-63a15ed1a2f9

just in case paper event based digital orensic investiagation framework 
https://www.dfrws.org/sites/default/files/session-files/paper-an_event-based_digital_forensic_investigation_framework.pdf