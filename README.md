# Responding to Ethereum Web-Application Attacks  

### Franklyn Josef Sciberras
### Supervisor: Dr Mark Joseph Vella
#### B.Sc. Computing Science (Hons.)  

DApps provide a solution where mistrusting entities are allowed to interact 
through a user-friendly environment, backed up by a security critical 
technology, more specifically the Ethereum blockchain. Given the setup,
security guarantees that the blockchain offers are often misunderstood to cater 
for all possible threats. However, the added front-end interfacing components 
fall outside the scope of these guarantees and consequently increase the 
possible attack surface.

The first part of this work attempts to investigate how the added front-end 
components can be exploited to undermine the present architecture. Assuming that
architectures such as crypto wallets cannot be compromised and private keys are
not disclosed, we study the possibility of abusing DApp behaviour without the 
need to hijack security critical tokens. This led to the identification of a set
of potential vulnerabilities, which can be studied within the optic of the 
following dimensions: 1) Exploitation of transactions through tampering and 
forgery; 2) Exploitation of the DApps functionality, either related to the 
nature of the application or cryptocurrency transfers; and 3) The implications 
that the DApp Browser in use has on the identified vulnerabilities. Based on 
this threat analysis, it was concluded that through DOM abuse, DApp 
functionality, and consequently transactions generated, can be compromised. 
However, the success of the exploit depends on the amount of visibility and 
control, the DApp Browser provides during the transaction authorization process. 

Considering that these types of attacks can go undetected for a number of days,
traditional memory forensic techniques would be hindered in this scenario.
For this reason, DAppInvestigator was proposed, with the aim of providing an 
effective solution that targets these needs. Using the technique we show that 
proactive system forensics revolving around dumping DOM-related information, 
driven by particular events emitted by the DApp Browser, can actually recreate 
the steps of eventual attacks and present them in a forensic timeline. 
The potential of this approach was demonstrated using two implementations that 
require a different level of TCB. In using a TCB that consists of the entire web
browser stack and a second version which only relies on DevTools Protocol server
code of the browser, we manage to set a solid basis for response. 


**The most important non-coding artefacts produced for this dissertation can be found in
_FinalThesisArtefacts_. The remaining coding artefacts can be found within their respective 
repositories, with the concerned repositores having prefix of _Thesis_ in the assigned title.**