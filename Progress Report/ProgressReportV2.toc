\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Aim}{2}
\contentsline {section}{\numberline {3}Objectives}{2}
\contentsline {section}{\numberline {4}A defense for why the problem is non-trivial}{2}
\contentsline {section}{\numberline {5}Background}{2}
\contentsline {subsection}{\numberline {5.1}Related Work}{3}
\contentsline {section}{\numberline {6}DApp Architectural Analysis}{4}
\contentsline {section}{\numberline {7}Plan of Work}{4}
\contentsline {section}{\numberline {8}Proposed Solution}{4}
\contentsline {section}{\numberline {9}Proposed Evaluation}{5}
\contentsline {section}{\numberline {10}References}{6}
