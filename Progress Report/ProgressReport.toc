\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Aim and Objectives}{2}
\contentsline {subsection}{\numberline {2.1}A defense for why the problem is non-trivial}{3}
\contentsline {section}{\numberline {3}Background}{3}
\contentsline {subsection}{\numberline {3.1}Typical Make-Up of DApp}{4}
\contentsline {subsection}{\numberline {3.2}Related Work}{5}
\contentsline {section}{\numberline {4}DApp Architectural Analysis}{5}
\contentsline {section}{\numberline {5}Plan of Work}{6}
\contentsline {section}{\numberline {6}Proposed Evaluation}{7}
\contentsline {section}{\numberline {7}Deliverables}{7}
\contentsline {section}{\numberline {8}References}{8}
